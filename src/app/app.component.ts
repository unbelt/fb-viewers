import { Component, OnInit, ViewContainerRef } from '@angular/core';

import { Constants, Templates } from './config';
import { Profile, IProfile } from './../models/profile.model';

import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import * as DirtyJson from 'dirty-json';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public profiles: IProfile[];
  public numbers: string[];
  public donateButton: string;
  public loading: boolean;

  private documentData: string;

  constructor(private toastr: ToastsManager, private vcr: ViewContainerRef) {
    toastr.setRootViewContainerRef(vcr);
    this.numbers = Templates.numbers;
    this.donateButton = Templates.donateButton;
    this.loading = true;
  }

  public ngOnInit(): void {
    this.init();

    setTimeout(() => {
      this.loading = false;

      if (this.profiles.length === 0) {
        this.toastr.info('Try to <span class="bold">refresh</span> your facebook.', 'No results found!');
      }
    }, 350);
  }

  public goToPage(url?: string): void {
    chrome.tabs.create({
      url: url || Constants.FACEBOOK_URL
    });
  }

  public reload(url?: string): void {
    chrome.tabs.update({
      url: url || Constants.FACEBOOK_URL
    });

    window.close();
  }

  private init(): void {
    chrome.tabs.query({
      'active': true,
      'windowId': chrome.windows.WINDOW_ID_CURRENT
    }, (tabs: chrome.tabs.Tab[]) => {
      const currentUrl = tabs[0].url;

      if (currentUrl && currentUrl.indexOf(Constants.FACEBOOK.toLowerCase()) > -1) {
        chrome.tabs.executeScript({
          code: `(${this.getDOM})();`
        }, (results: string[]) => {
          this.loading = false;
          this.documentData = results[0];
          this.profiles = this.getProfiles();
        });
      } else {
        this.loading = false;
        this.toastr.warning('Please select facebook tab.', 'Not on facebook!');
      }
    });
  }

  private getDOM(): string {
    return document.body.innerHTML;
  }

  private getProfilesIds(): string[] {
    let profileIds = [];

    try {
      profileIds = JSON.parse(this.documentData.substring(
        this.documentData.indexOf(Constants.LIST) + 8,
        this.documentData.indexOf(Constants.SHORT_PROFILES) - 1
      )).map(x => x.split('-')[0]).filter((elem, pos, arr) => {
        return arr.indexOf(elem) === pos;
      });

      return profileIds;

    } catch (ex) {
      this.toastr.error('Not logged in?', 'Something went wrong!');
    }
  }

  private getProfiles(): IProfile[] {
    // const unorderedProfiles = [];

    const shortProfiles = this.documentData.substring(
      this.documentData.indexOf(Constants.SHORT_PROFILES) + 14,
      this.documentData.indexOf(Constants.NEARBY) - 1
    ).replace(/"\d+":/g, '').slice(1, -1);
    // .split(/(.*?},)/g)
    // .filter(Boolean)
    // .map(p => {
    //   DirtyJson.parse(p.slice(0, -1)).then((profile: IProfile) => {
    //     if (profile.name) {
    //       unorderedProfiles.push(new Profile(profile));
    //     }
    //   });
    // });

    if (!shortProfiles) {
      return;
    }

    const profilesIds = this.getProfilesIds();

    if (!profilesIds) {
      return;
    }

    const profiles = [];
    let profileStr;

    for (let i = 0; i < profilesIds.length; i++) {
      profileStr = shortProfiles.substring(
        shortProfiles.indexOf(profilesIds[i]) - 5,
        shortProfiles.length
      ).split('},')[0];

      try {
        DirtyJson.parse(profileStr + '}').then((profile: IProfile) => {
          if (profile.name && !profiles.find(p => p.name === profile.name)) {
            profiles.push(profile);
          }
        });
      } catch (ex) {
        console.log(ex);
      }
    }


    return profiles;
  }
}
