export interface IProfile {
    id: number;
    name: string;
    firstName: string;
    vanity: string;
    thumbSrc: string;
    uri: string;
    gender: number;
    type: string;
    is_friend: boolean;
    alternateName: string;
    is_nonfriend_messenger_contact: boolean;
}

export class Profile implements IProfile {
    public id: number;
    public name: string;
    public firstName: string;
    public vanity: string;
    public thumbSrc: string;
    public uri: string;
    public gender: number;
    public type: string;
    public is_friend: boolean;
    public alternateName: string;
    public is_nonfriend_messenger_contact: boolean;

    constructor(obj?: any) {
        this.id = obj && obj.id;
        this.name = obj && obj.name;
        this.firstName = obj && obj.firstName;
        this.vanity = obj && obj.vanity;
        this.thumbSrc = obj && obj.thumbSrc;
        this.uri = obj && obj.uri;
        this.gender = obj && obj.gender;
        this.type = obj && obj.type;
        this.is_friend = obj && obj.is_friend;
        this.alternateName = obj && obj.alternateName;
        this.is_nonfriend_messenger_contact = obj && obj.is_nonfriend_messenger_contact;
    }
}
